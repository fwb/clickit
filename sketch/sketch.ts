import p5 from "p5";

import { Globals } from './globals';
import { Particle } from './particle';
import { Target } from './target';
import { Timer } from './timer';

var ref: p5.Image;
var target: Target;
var defaultColor: p5.Color;
var collideColor: p5.Color;
var score: number;
var clicks: number;
var font: p5.Font;
var fontColor: p5.Color;
var velocity: number;
var vSlider: p5.Element;
var timer: Timer;
var particles: Particle[];
// var collideColor: p5.Color;
// var defaultColor: p5.Color;

var clickit = function (sketch: p5) {
    // TODO: headshot game
    // x click on dot to score
    // x change color on click
    // scoreboard at top
    // reset game button at bottom
    // configure colors
    // play sound and animation on click
    // style and position slider
    // mobile/touch support

    sketch.preload = function () {
        font = sketch.loadFont('fonts/BalooBhaina2-Medium.ttf');
        ref = sketch.loadImage('images/TedPicture.png');
    }

    sketch.setup = function () {
        var cnv = sketch.createCanvas(sketch.windowWidth, sketch.windowHeight)
        cnv.style('display', 'block');
        cnv.parent('canvas-container');

        sketch.cursor('crosshair');

        sketch.textFont(font);
        sketch.textSize(20);
        fontColor = sketch.color(0, 128, 255);

        target = new Target(sketch.windowWidth, sketch.windowHeight, Globals.TargetSize, sketch);
        score = 0;
        clicks = 0;

        velocity = Globals.DefaultVelocity;
        vSlider = sketch.createSlider(10, 255, Globals.DefaultVelocity);
        vSlider.position(sketch.windowWidth / 2, sketch.windowHeight - 35);

        timer = new Timer();

        particles = new Array<Particle>();

        sketch.ellipseMode(sketch.CENTER);

        sketch.background(Globals.DefaultBackground);
        sketch.frameRate(Globals.FrameRate);
    }

    sketch.draw = function () {
        const v = vSlider.value();
        if (v !== velocity) {
            if (typeof (v) === "number") {
                velocity = v;
            } else {
                velocity = 0;
            }
            target.setVelocity(velocity);
            console.log({ velocity });
        }

        target.update();
        for (let p of particles) {
            p.update();
            // TODO: particle lifetime
        }

        // sketch.background(Globals.DefaultBackground);
        target.draw();
        for (let p of particles) {
            p.draw();
        }

        if (timer.active) {
            timer.tick(sketch.deltaTime);
        }

        sketch.fill(fontColor);
        sketch.text(`Score: ${score} | Clicks: ${clicks}`, 5, sketch.windowHeight - 5);
    }

    sketch.touchEnded = function () {
        if (timer.active) {
            return;
        } else {
            timer.start();
        }

        this.clicks++;
        if (target.collides(sketch.mouseX, sketch.mouseY)) {
            if (target.clicked()) {
                this.score += 1;
                const p = new Particle(ref, sketch);
                p.spawn();
                particles.push(p);
            }
        }
    }

    sketch.windowResized = function () {
        sketch.resizeCanvas(sketch.windowWidth, sketch.windowHeight);
    }
};

new p5(clickit);
