// import p5 from 'p5';

export class Transform {
    // class Transform {
    pos: p5.Vector;
    size: number;

    constructor(sketch: p5, x = 0, y = 0, size = 1) {
        this.pos = sketch.createVector(x, y);
        this.size = size;
    }
}