const fs = require('fs');
const path = require('path');

module.exports = {
    cert: fs.readFileSync(path.join(__dirname, '/../local/ls.pem')),
    key: fs.readFileSync(path.join(__dirname, '/../local/ls.key')),
};